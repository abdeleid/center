<?php

return [
    'home' => 'Home',
    'logout' => 'logout',
    'login' => 'login',

    'created_at' => 'Created At',

    'all' => 'All',
    'add' => 'Add',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'bulk_delete' => 'Bulk Delete',
    'action' => 'Actions',
    'search' => 'Search',
    'no_data_found' => 'Sorry no records found',
    'added_successfully' => 'Data added successfully',
    'updated_successfully' => 'Data edited successfully',
    'deleted_successfully' => 'Data deleted successfully',
    'confirm_delete' => 'Confirm Deletion',
    'yes' => 'yes',
    'no' => 'no',
    'hi' => 'hi',
    'choose' => 'choose',
    'create' => 'Create',
    'read' => 'read',
    'update' => 'update',
    'delete' => 'Delete',
    'export' => 'Export',

];