<?php

return [
    'users' => 'Users',
    'user' => 'User',
    'teachers' => 'Teachers',
    'teacher' => 'Teacher',
    'first_name' => 'First Name',
    'last_name' => 'Last Name',
    'email' => 'Email',
    'password' => 'Password',
    'password_confirmation' => 'Password Confirmation',
];
