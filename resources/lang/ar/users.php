<?php

return [
    'users' => 'المستخدمين',
    'user' => 'المستخدم',
    'name' => 'السم',
    'email' => 'البريد الاكتروني',
    'password' => 'كلمه المرور',
    'password_confirmation' => 'تاكيد كلمه المرور',
];
