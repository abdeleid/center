<?php

return [
    'home' => 'الرئسيه',
    'logout' => 'تسجيل الخروج',
    'login' => 'تسجيل الدخول',

    'created_at' => 'تم الاضافه في',

    'all' => 'كل',
    'add' => 'اضف',
    'edit' => 'تعديل',
    'delete' => 'حذف',
    'bulk_delete' => 'حذف متعدد',
    'action' => 'اكشن',
    'search' => 'بحث',
    'no_data_found' => 'للاسف لايوحد اي سجلات',
    'added_successfully' => 'تم اضافه السجلات بنجاح',
    'updated_successfully' => 'تم تعديل السجلات بنجاح',
    'deleted_successfully' => 'تم حذف السجلات بنجاح',
    'confirm_delete' => 'تاكيد الحذف',
    'yes' => 'نعم',
    'no' => 'لا',
    'hi' => 'اهلا',
    'choose' => 'اختر',
    'create' => 'اضافه',
    'read' => 'عرض',
    'update' => 'تعديل',
    'delete' => 'حذف',

];