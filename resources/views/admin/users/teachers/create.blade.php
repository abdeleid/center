@extends('layouts.admin.app')

@section('content')

    <div>
        <h2>@lang('users.teachers')</h2>
    </div>

    <ul class="breadcrumb mt-2">
        <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">@lang('site.home')</a></li>
        <li class="breadcrumb-item"><a href="{{ route('admin.users.index', ['account_type' => 'teacher']) }}">@lang('users.teacher')</a></li>
        <li class="breadcrumb-item">@lang('site.add')</li>
    </ul>

    <div class="row">

        <div class="col-md-12">

            <div class="tile shadow">

                @include('admin.partials._errors')

                <form action="{{ route('admin.users.store') }}" method="post">
                    @csrf
                    @method('post')

                    <input type="hidden" name="account_type" value="teacher">

                    {{--first_name--}}
                    <div class="form-group">
                        <label>@lang('users.first_name')</label>
                        <input type="text" name="first_name" class="form-control" autofocus value="{{ old('first_name') }}">
                    </div>

                    {{--last_name--}}
                    <div class="form-group">
                        <label>@lang('users.last_name')</label>
                        <input type="last_name" name="last_name" class="form-control" value="{{ old('last_name') }}">
                    </div>

                    {{--email --}}
                    <div class="form-group">
                        <label>@lang('users.email')</label>
                        <input type="email" name="email" class="form-control" value="{{ old('email') }}">
                    </div>

                    {{--password--}}
                    <div class="form-group">
                        <label>@lang('users.password')</label>
                        <input type="password" name="password" class="form-control" value="">
                    </div>

                    {{--password confirmation--}}
                    <div class="form-group">
                        <label>@lang('users.password_confirmation')</label>
                        <input type="password" name="password_confirmation" class="form-control" value="">
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i>@lang('site.add')</button>
                    </div>

                </form><!-- end of form -->

            </div><!-- end of tile -->

        </div><!-- end of col -->

    </div><!-- end of row -->

@endsection
