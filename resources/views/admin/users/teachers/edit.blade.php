@extends('layouts.admin.app')

@section('content')

    <div>
        <h2>@lang('teachers.teachers')</h2>
    </div>

    <ul class="breadcrumb mt-2">
        <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">@lang('site.home')</a></li>
        <li class="breadcrumb-item"><a href="{{ route('admin.users.index', ['account_type' => $user->account_type]) }}">@lang('teachers.teachers')</a></li>
        <li class="breadcrumb-item">@lang('site.edit')</li>
    </ul>

    <div class="row">

        <div class="col-md-12">

            <div class="tile shadow">

                @include('admin.partials._errors')

                <form method="post" action="{{ route('admin.users.update', $user->id) }}">
                    @csrf
                    @method('put')

                    {{--first_name--}}
                    <div class="form-group">
                        <label>@lang('users.first_name')</label>
                        <input type="text" name="first_name" class="form-control" autofocus value="{{ old('first_name', $user->first_name) }}">
                    </div>

                    {{--last_name--}}
                    <div class="form-group">
                        <label>@lang('users.last_name')</label>
                        <input type="last_name" name="last_name" class="form-control" value="{{ old('last_name', $user->last_name) }}">
                    </div>

                    {{--email --}}
                    <div class="form-group">
                        <label>@lang('users.email')</label>
                        <input type="email" name="email" class="form-control" value="{{ old('email', $user->email) }}">
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-edit"></i>@lang('site.edit')</button>
                    </div>

                </form><!-- end of form -->

            </div><!-- end of tile -->

        </div><!-- end of col -->

    </div><!-- end of row -->

@endsection
