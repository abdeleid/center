@extends('layouts.admin.app')

@section('content')

    <h2 class="mb-2">@lang('courses.courses')</h2>

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-gray-light">
            <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">@lang('site.home')</a></li>
            <li class="breadcrumb-item active" aria-current="page">@lang('courses.courses')</li>
        </ol>
    </nav>

    <div class="row">

        <div class="col-md-12">

            <div class="block">

                <div class="block-content block-content-full">

                    <div class="row mb-2">

                        <div class="col-md-12">
                            @if (auth()->user()->hasPermission('create_courses'))
                                <a href="{{ route('admin.courses.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> @lang('site.add')</a>
                            @endif
                            @if (auth()->user()->hasPermission('delete_courses'))
                                <form method="post" action="{{ route('admin.courses.bulk_delete') }}" style="display: inline-block;">
                                    @csrf
                                    @method('delete')
                                    <input type="hidden" name="record_ids" id="record-ids">
                                    <button type="submit" class="btn btn-danger" id="bulk-delete" disabled="true"><i class="fa fa-trash"></i> @lang('site.bulk_delete')</button>
                                </form><!-- end of form -->
                            @endif
                        </div>

                    </div><!-- end of row -->

                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" id="data-table-search" class="form-control" autofocus placeholder="@lang('site.search')">
                            </div>
                        </div>

                    </div><!-- end of row -->

                    <div class="row">

                        <div class="col-md-12 table-responsive">
                            <table class="table" id="courses-table" style="width: 100%;">
                                <thead>
                                <tr>
                                    <th>
                                        <label class="css-control css-control-primary css-checkbox">
                                            <input type="checkbox" class="css-control-input" id="record__select-all">
                                            <span class="css-control-indicator"></span>
                                        </label>
                                    </th>
                                    <th>@lang('courses.name')</th>
                                    <th>@lang('users.teacher')</th>
                                    <th>@lang('site.created_at')</th>
                                    <th>@lang('site.action')</th>
                                </tr>
                                </thead>
                            </table>
                        </div>

                    </div><!-- end of row -->

                </div><!-- end of block content -->

            </div><!-- end of block-->

        </div><!-- end of col -->

    </div><!-- end of row -->

@endsection

@push('scripts')

    <script>

        let coursesTable = $('#courses-table').DataTable({
            dom: "tiplr",
            serverSide: true,
            processing: true,
            ajax: {
                url: '{{ route('admin.courses.data') }}',
            },
            columns: [
                {data: 'record_select', name: 'record_select', searchable: false, sortable: false, width: '1%'},
                {data: 'name', name: 'name'},
                {data: 'teacher', name: 'teacher'},
                {data: 'created_at', name: 'created_at', searchable: false},
                {data: 'actions', name: 'actions', searchable: false, sortable: false},
            ],
            order: [[1, 'asc']],
            drawCallback: function (settings) {
                $('.record__select').prop('checked', false);
                $('#record__select-all').prop('checked', false);
                $('#record-ids').val();
                $('#bulk-delete').attr('disabled', true);

            }
        });

        $('#data-table-search').keyup(function () {
            coursesTable.search(this.value).draw();
        })
    </script>

@endpush