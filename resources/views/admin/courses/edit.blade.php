@extends('layouts.admin.app')

@section('content')

    <h2 class="mb-2">@lang('courses.courses')</h2>

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-gray-light">
            <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{ route('admin.courses.index') }}">@lang('courses.courses')</a></li>
            <li class="breadcrumb-item active" aria-current="page">@lang('site.edit')</li>
        </ol>
    </nav>

    <div class="row">

        <div class="col-md-12">

            <div class="block">

                <div class="block-content block-content-full">

                    @include('admin.partials._errors')

                    <form method="post" action="{{ route('admin.courses.update', $course->id) }}">
                        @csrf
                        @method('put')

                        {{--name--}}
                        <div class="form-group">
                            <label>@lang('courses.name')</label>
                            <input type="text" name="name" class="form-control" autofocus value="{{ old('name', $course->name) }}">
                        </div>

                        {{--teacher_id--}}
                        <div class="form-group">
                            <label>@lang('users.teacher')</label>
                            <select name="teacher_id" class="form-control select2">
                                <option value="">@lang('site.choose') @lang('users.teacher')</option>
                                @foreach ($teachers as $teacher)
                                    <option value="{{ $teacher->id }}" {{ $teacher->id == $course->teacher_id ? 'selected' : '' }}>{{ $teacher->full_name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-edit"></i>@lang('site.edit')</button>
                        </div>

                    </form><!-- end of form -->

                </div><!-- end of block content -->

            </div><!-- end of tile -->

        </div><!-- end of col -->

    </div><!-- end of row -->

@endsection
