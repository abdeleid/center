{{--<div class="animated-checkbox">--}}
{{--<label class="m-0">--}}
{{--<input type="checkbox" class="record__select" value="{{ $id }}">--}}
{{--<span class="label-text"></span>--}}
{{--</label>--}}
{{--</div>--}}

<label class="css-control css-control-primary css-checkbox">
    <input type="checkbox" class="css-control-input record__select" value="{{ $id }}">
    <span class="css-control-indicator"></span>
</label>