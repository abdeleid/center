<div class="dropdown">
    <button class="btn btn-primary dropdown-toggle js-tooltip" type="button" id="dropdownMenuButton" data-toggle="dropdown" title="soso" aria-haspopup="true" aria-expanded="false">
        <span class="fa fa-ellipsis-v"></span>
    </button>
    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
        @if (auth()->user()->hasPermission('update_courses'))
            <a class="dropdown-item" href="{{ route('admin.courses.edit', $id) }}">
                <span class="la la-edit"></span>
                @lang('site.edit')
            </a>
        @endif
        @if (auth()->user()->hasPermission('delete_courses'))
            <a class="dropdown-item delete" href="#">
                <span class="la la-trash"></span>
                @lang('site.delete')
            </a>
            <form method="post" action="{{ route('admin.courses.destroy', $id) }}" id="delete-form">
                @csrf
                @method('delete')
            </form><!-- end of form -->
        @endif
    </div>
</div>