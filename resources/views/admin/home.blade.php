@extends('layouts.admin.app')

@section('content')

    <h2 class="mb-2">@lang('site.home')</h2>

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-gray-light">
            <li class="breadcrumb-item active" aria-current="page">@lang('site.home')</li>
        </ol>
    </nav>


@endsection