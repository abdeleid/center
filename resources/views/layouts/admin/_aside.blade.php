<div class="content-header content-header-fullrow px-15">
    <!-- Mini Mode -->
    <div class="content-header-section sidebar-mini-visible-b">
        <!-- Logo -->
        <span class="content-header-item font-w700 font-size-xl float-right animated fadeIn">
                        <span class="text-dual-primary-dark">c</span><span class="text-primary">b</span>
                    </span>
        <!-- END Logo -->
    </div>

    <!-- Normal Mode -->
    <div class="content-header-section text-center align-parent sidebar-mini-hidden">
        <!-- Close Sidebar, Visible only on mobile screens -->
        <!-- Layout API, functionality initialized in Codebase() -> uiApiLayout() -->
        <button type="button" class="btn btn-circle btn-dual-secondary d-lg-none align-v-l" data-toggle="layout" data-action="sidebar_close">
            <i class="fa fa-times text-danger"></i>
        </button>
        <!-- END Close Sidebar -->

        <!-- Logo -->
        <div class="content-header-item">
            <a class="link-effect font-w700" href="">
                <i class="si si-fire text-primary"></i>
                <span class="font-size-xl text-dual-primary-dark">code</span><span class="font-size-xl text-primary">base</span>
            </a>
        </div>
        <!-- END Logo -->
    </div>
    <!-- END Normal Mode -->
</div>

<div class="content-side content-side-full">
    <ul class="nav-main">
        <li>
            <a href="{{ route('admin.home') }}">
                <i class="la la-home"></i>
                <span class="sidebar-mini-hide">@lang('site.home')</span>
            </a>
        </li>

        <li>
            <a href="{{ route('admin.courses.index') }}">
                <i class="la la-certificate"></i>
                <span class="sidebar-mini-hide">Courses</span>
            </a>
        </li>

        {{--<li>--}}
        {{--<a class="nav-submenu" data-toggle="nav-submenu" href="#">--}}
        {{--<i class="si si-puzzle"></i>--}}
        {{--<span class="sidebar-mini-hide">Dropdown</span>--}}
        {{--</a>--}}
        {{--<ul>--}}
        {{--<li>--}}
        {{--<a href="javascript:void(0)">Link #1</a>--}}
        {{--</li>--}}
        {{--<li>--}}
        {{--<a href="javascript:void(0)">Link #2</a>--}}
        {{--</li>--}}
        {{--</ul>--}}
        {{--</li>--}}
    </ul>
</div>