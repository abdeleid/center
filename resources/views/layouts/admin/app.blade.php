<!doctype html>
<html lang="en" class="no-focus" dir="">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

    <title>Codebase - Bootstrap 4 Admin Template &amp; UI Framework</title>

    <meta name="description" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework created by pixelcave and published on Themeforest">
    <meta name="author" content="pixelcave">
    <meta name="robots" content="noindex, nofollow">

    <meta property="og:title" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework">
    <meta property="og:site_name" content="Codebase">
    <meta property="og:description" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework created by pixelcave and published on Themeforest">
    <meta property="og:type" content="website">
    <meta property="og:url" content="">
    <meta property="og:image" content="">

    <link rel="shortcut icon" href="{{ asset('admin_assets//media/favicons/favicon.png') }}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{ asset('admin_assets//media/favicons/favicon-192x192.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('admin_assets/media/favicons/apple-touch-icon-180x180.png') }}">

    {{--line awesome--}}
    <link rel="stylesheet" href="{{ asset('admin_assets/css/line-awesome.min.css') }}">

    {{--datatable--}}
    <link rel="stylesheet" href="{{ asset('admin_assets/js/plugins/datatables/dataTables.bootstrap4.css') }}">

    {{--select2--}}
    <link rel="stylesheet" href="{{ asset('admin_assets/js/plugins/select2/css/select2.min.css') }}">

    {{--noty --}}
    <link rel="stylesheet" href="{{ asset('admin_assets/js/plugins/noty/noty.css') }}">
    <script src="{{ asset('admin_assets/js/plugins/noty/noty.min.js') }}"></script>

    {{--gogle fonts ltr--}}
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,400i,600,700&display=swap">
    <link rel="stylesheet" id="css-main" href="{{ asset('admin_assets/css/codebase.min.css') }}">

    {{--custom styles--}}
    <link rel="stylesheet" id="css-main" href="{{ asset('admin_assets/css/custom.css') }}">

    @if (app()->getLocale() == 'ar')
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Cairo:400,600&display=swap">
        <link rel="stylesheet" id="css-main" href="{{ asset('admin_assets/css/codebase-rtl.css') }}">
        <style>
            body {
                font-family: 'cairo', 'sans-serif' !important;
            }
        </style>
    @endif

</head>
<body>
<div id="page-container" class="sidebar-o {{ app()->getLocale() == 'ar' ? 'sidebar-r' : '' }} sidebar-inverse side-scroll page-header-modern main-content-boxed enable-page-overlay">


    <nav id="sidebar">
        <div class="sidebar-content">
            @include('layouts.admin._aside')
        </div>
    </nav>

    @include('layouts.admin._header')

    <main id="main-container">

        <div class="content">
            @include('admin.partials._session')
            @yield('content')
        </div>

    </main>

    <!-- Footer -->
    <footer id="page-footer" class="opacity-0">
        <div class="content py-20 font-size-sm clearfix">
            <div class="float-right">
                <a class="font-w600" href="https://1.envato.market/95j" target="_blank">Codebase</a> &copy; <span class="js-year-copy"></span>
            </div>
            <div class="float-left">
                Crafted with <i class="fa fa-heart text-pulse"></i> by <a class="font-w600" href="https://1.envato.market/ydb" target="_blank">pixelcave</a>
            </div>
        </div>
    </footer>
    <!-- END Footer -->
</div>
<!-- END Page Container -->

<!-- Codebase JS -->
<script src="{{ asset('admin_assets/js/codebase.core.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/codebase.app.min.js') }}"></script>

{{--datatable--}}
<script src="{{ asset('admin_assets/js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>

{{--select2--}}
<script src="{{ asset('admin_assets/js/plugins/select2/js/select2.full.min.js') }}"></script>

{{--custom--}}
<script src="{{ asset('admin_assets/js/custom/index.js') }}"></script>

<script>
    $(document).ready(function () {

        //bulk delete
        $(document).on('click', '.delete ,#bulk-delete', function (e) {

            var that = $(this)

            e.preventDefault();

            var n = new Noty({
                text: "@lang('site.confirm_delete')",
                type: "alert",
                killer: true,
                buttons: [
                    Noty.button("@lang('site.yes')", 'btn btn-success mr-2', function () {
                        if (that.hasClass('delete')) {
                            that.parent().find('form').submit();
                        }
                        that.closest('form').submit();
                    }),

                    Noty.button("@lang('site.no')", 'btn btn-danger mr-2', function () {
                        n.close();
                    })
                ]
            });

            n.show();

        });//end of delete

    });//end of document ready

</script>

@stack('scripts')
</body>
</html>
