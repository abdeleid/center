<?php

Route::prefix('admin')->name('admin.')->middleware(['auth', 'role:super_admin|admin'])->group(function () {

    Route::get('/home', 'HomeController@index')->name('home');

    //course routes
    Route::get('/courses/data', 'CourseController@data')->name('courses.data');
    Route::delete('/courses/bulk_delete', 'CourseController@bulkDelete')->name('courses.bulk_delete');
    Route::resource('courses', 'CourseController');

    //teacher routes
    Route::get('/users/data', 'UserController@data')->name('users.data');
    Route::delete('/users/bulk_delete', 'UserController@bulkDelete')->name('users.bulk_delete');
    Route::resource('users', 'UserController');
});
