<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class UserController extends Controller
{
    public function index()
    {
        switch (request()->account_type) {
            case 'teacher':
                return view('admin.users.teachers.index');

            case 'student':
                return view('admin.users.students.index');

        }//end of switch

    }// end of index

    public function data()
    {
        $users = User::whenAccountType(request()->account_type);

        return DataTables::of($users)
            ->addColumn('record_select', 'admin.users.data_table.record_select')
            ->editColumn('created_at', function (User $user) {
                return $user->created_at->format('Y-m-d');
            })
            ->addColumn('actions', 'admin.users.data_table.actions')
            ->rawColumns(['record_select', 'created_at', 'actions'])
            ->toJson();

    }// end of data

    public function create()
    {
        switch (request()->account_type) {
            case 'teacher':
                return view('admin.users.teachers.create');

            case 'student':
                return view('admin.users.students.create');

            case 'admin':
                return view('admin.users.admins.create');

        }//end of switch

    }// end of create

    public function store(Request $request)
    {
        switch ($request->account_type) {
            case 'teacher':
                $validatedData = $request->validate([
                    'first_name' => 'required',
                    'last_name' => 'required',
                    'email' => 'required|unique:users,email',
                    'password' => 'required|confirmed',
                ]);
                $validatedData['password'] = bcrypt($validatedData['password']);
                $validatedData['account_type'] = 'teacher';
                break;

        }//end of switch

        $user = User::create($validatedData);
        session()->flash('success', __('site.added_successfully'));

        return redirect()->route('admin.users.index', ['account_type' => $user->account_type]);

    }// end of store

    public function edit(User $user)
    {
        switch ($user->account_type) {
            case 'teacher':
                return view('admin.users.teachers.edit', compact('user'));

            case 'student':
                return view('admin.users.students.edit', compact('user'));

            case 'admin':
                return view('admin.users.admins.edit', compact('user'));

        }//end of switch

    }// end of edit

    public function update(Request $request, User $user)
    {
        switch ($user->account_type) {
            case 'teacher':
                $validatedData = $request->validate([
                    'first_name' => 'required',
                    'last_name' => 'required',
                    'email' => 'required|unique:users,email,' . $user->id,
                ]);

                break;

        }//end of switch

        $user->update($validatedData);
        session()->flash('success', __('site.updated_successfully'));
        return redirect()->route('admin.users.index', ['account_type' => $user->account_type]);

    }// end of update

    public function destroy(User $user)
    {
        $this->delete($user);
        session()->flash('success', __('site.deleted_successfully'));
        return redirect()->route('admin.users.index', ['account_type' => $user->account_type]);

    }// end of destroy

    public function bulkDelete()
    {
        foreach (json_decode(request()->record_ids) as $recordId) {

            $user = User::FindOrFail($recordId);
            $this->delete($user);

        }//end of for each

        session()->flash('success', __('site.deleted_successfully'));
        return redirect()->route('admin.users.index', ['account_type' => $user->account_type]);

    }// end of bulkDelete

    private function delete(User $teacher)
    {
        $teacher->delete();

    }// end of delete


}//end of controller
