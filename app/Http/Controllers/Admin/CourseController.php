<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\User;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class CourseController extends Controller
{
    public function index()
    {
        return view('admin.courses.index');

    }// end of index

    public function data()
    {
        $courses = Course::select();

        return DataTables::of($courses)
            ->addColumn('record_select', 'admin.courses.data_table.record_select')
            ->addColumn('teacher', function (Course $course) {
                return $course->teacher->full_name;
            })
            ->editColumn('created_at', function (Course $course) {
                return $course->created_at->format('Y-m-d');
            })
            ->addColumn('actions', 'admin.courses.data_table.actions')
            ->rawColumns(['record_select', 'teacher', 'created_at', 'actions'])
            ->toJson();

    }// end of data

    public function create()
    {
        $teachers = User::where('account_type', 'teacher')->get();
        return view('admin.courses.create', compact('teachers'));

    }// end of create

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|unique:courses,name',
            'teacher_id' => 'required',
        ]);

        Course::create($validatedData);
        session()->flash('success', __('site.added_successfully'));
        return redirect()->route('admin.courses.index');

    }// end of store

    public function edit(Course $course)
    {
        $teachers = User::where('account_type', 'teacher')->get();
        return view('admin.courses.edit', compact('teachers', 'course'));

    }// end of edit

    public function update(Request $request, Course $course)
    {
        $validatedData = $request->validate([
            'name' => 'required|unique:courses,name,' . $course->id,
        ]);

        $course->update($validatedData);
        session()->flash('success', __('site.updated_successfully'));
        return redirect()->route('admin.courses.index');

    }// end of update

    public function destroy(Course $course)
    {
        $this->delete($course);
        session()->flash('success', __('site.deleted_successfully'));
        return redirect()->route('admin.courses.index');

    }// end of destroy

    public function bulkDelete()
    {
        foreach (json_decode(request()->record_ids) as $recordId) {

            $course = Course::FindOrFail($recordId);
            $this->delete($course);

        }//end of for each

        session()->flash('success', __('site.deleted_successfully'));
        return redirect()->route('admin.courses.index');

    }// end of bulkDelete

    private function delete(Course $course)
    {
        $course->delete();

    }// end of delete

}//end of controller
