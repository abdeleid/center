<?php

namespace App\Http\Controllers;

use App\Lib\WizIqHelper;

class ClassController extends WizIqHelper
{
    public function store()
    {
        $method = "create";
        $requestParameters["signature"] = $this->GenerateSignature($method, $requestParameters);

        $requestParameters["presenter_id"] = "12";
        $requestParameters["presenter_name"] = "teacher three";
        $requestParameters["start_time"] = "2020-06-30 1:40PM";
        $requestParameters["title"] = "physics-class"; //Required
        $requestParameters["duration"] = "15"; //optional
        $requestParameters["time_zone"] = "Africa/Cairo"; //optional
        $requestParameters["attendee_limit"] = ""; //optional
        $requestParameters["control_category_id"] = ""; //optional
        $requestParameters["create_recording"] = ""; //optional
        $requestParameters["return_url"] = ""; //optional
        $requestParameters["status_ping_url"] = ""; //optional
        $requestParameters["language_culture_name"] = "en-us";

        try {
            $XMLReturn = $this->doPostRequest($this->webServiceUrl . '?method=create', http_build_query($requestParameters, '', '&'));

        } catch (Exception $e) {

            echo $e->getMessage();
        }

        if (!empty($XMLReturn)) {

            try {

                $objDOM = new \DOMDocument();
                $objDOM->loadXML($XMLReturn);

            } catch (Exception $e) {

                echo $e->getMessage();

            }

            $status = $objDOM->getElementsByTagName("rsp")->item(0);
            $attribNode = $status->getAttribute("status");

            if ($attribNode == "ok") {
                dd($objDOM);
                $methodTag = $objDOM->getElementsByTagName("method");
                echo "method=" . $method = $methodTag->item(0)->nodeValue;
                $class_idTag = $objDOM->getElementsByTagName("class_id");
                echo "<br>Class ID=" . $class_id = $class_idTag->item(0)->nodeValue;
                $recording_urlTag = $objDOM->getElementsByTagName("recording_url");
                echo "<br>recording_url=" . $recording_url = $recording_urlTag->item(0)->nodeValue;
                // $presenter_emailTag = $objDOM->getElementsByTagName("presenter_email");
                // echo "<br>presenter_email=" . $presenter_email = $presenter_emailTag->item(0)->nodeValue;
                $presenter_urlTag = $objDOM->getElementsByTagName("presenter_url");
                echo "<br>presenter_url=" . $presenter_url = $presenter_urlTag->item(0)->nodeValue;
            } else if ($attribNode == "fail") {
                $error = $objDOM->getElementsByTagName("error")->item(0);
                echo "<br>errorcode=" . $errorcode = $error->getAttribute("code");
                echo "<br>errormsg=" . $errormsg = $error->getAttribute("msg");
            }

        }//end if

    }// end of store

}//end of store
