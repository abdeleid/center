<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laratrust\Traits\LaratrustUserTrait;

class User extends Authenticatable
{
    use LaratrustUserTrait;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'account_type',
    ];

    public function getFullNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;

    }// end of getFullNameAttribute

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    //scopes
    public function scopeWhenAccountType($query, $accountType)
    {
        return $query->when($accountType, function ($q) use ($accountType) {

            return $q->where('account_type', $accountType);

        });

    }// end of scopeWhenAccountType

}//end of model
