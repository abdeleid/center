<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = ['teacher_id', 'name'];

    //attr
    //scope
    //rel
    public function teacher()
    {
        return $this->belongsTo(User::class, 'teacher_id');

    }// end of teacher

}//end of model
